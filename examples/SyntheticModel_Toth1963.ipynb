{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "# An iMODFLOW model from synthetic data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "This notebook illustrates how to setup a simple two-dimensional groundwater model using synthetic data for conductivity and boundary conditions. We assume you have some familiarity with iMODFLOW files and model concepts. This notebook shows a very basic workflow:\n",
    "1. Generate some data\n",
    "2. Store these data in `xarray.DataArray`s\n",
    "3. Inspect the data using `matplotlib` and `xarray`'s built-in plotting functionality\n",
    "4. Mutate some data inside a `DataArray`\n",
    "5. Store the `DataArray`s in `OrderedDict` and use the `imod` package to write files so iMODFLOW can run it\n",
    "6. After running the model (outside of the notebook), load the results\n",
    "7. Finally, plot the results.\n",
    "\n",
    "Basic equations and parameter values have been taken from these two papers: \n",
    "* Toth, 1963, A Theoretical Analysis of Groundwater Flow in Small Drainage Basins\n",
    "* Xiao-Wei Jiang, Li Wan, Xu-Sheng Wang, Shemin Ge, and Jie Liu, 2008, Effect of exponential decay in hydraulic conductivity with depth on regional groundwater flow"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from collections import OrderedDict\n",
    "\n",
    "import imod\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import xarray as xr\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "The head of the phreatic water table is given by:\n",
    "\n",
    "$z_x(x) = z_0 + x \\tan\\alpha + \\frac{a}{\\cos(\\alpha)} \\sin(\\frac{bx}{\\cos \\alpha})$ <div style=\"text-align: right\"> (Toth, 1963) </div>\n",
    "\n",
    "Conductivity decreases exponentially with depth, according to:\n",
    "\n",
    "$k(z) = k_0 \\exp[-A (z_s - z)]$ <div style=\"text-align: right\"> (Jiang et al., 2009) </div>\n",
    "\n",
    "As Python functions, this becomes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "def phreatic_head(z0, x, a, alpha, b):\n",
    "    \"\"\"Synthetic ground surface, a la Toth 1963\"\"\"\n",
    "    return (z0 + x * np.tan(alpha) + a / np.cos(alpha) \n",
    "            * np.sin((b * x) / (np.cos(alpha))))\n",
    "\n",
    "\n",
    "def conductivity(k0, z, A):\n",
    "    \"\"\"Exponentially decaying conductivity\"\"\"\n",
    "    return k0 * np.exp(-A * (1000.0 - z))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Generate the phreatic boundary condition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "We start by defining the parameters for the `phreatic_head` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "z0 = 1000.0\n",
    "x = np.arange(0.0, 6000.0, 10.0) + 5.0\n",
    "a = 15.0\n",
    "b = np.pi / 750.0\n",
    "alpha = 0.02"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Next, we create an `xarray.DataArray` to hold the phreatic head."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "head_top = xr.DataArray(\n",
    "    data=phreatic_head(z0, x, a, alpha, b),\n",
    "    coords={\"x\": x},\n",
    "    dims=(\"x\")\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Let's have a look at whether that worked out, by plotting the result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(10, 5))\n",
    "head_top.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Generate conductivity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "We're going to setup the IBOUND DataArray first. We can use it as a template afterwards to generate the other arrays, like conducitivies and starting heads."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "ncol = x.size\n",
    "nrow = 1\n",
    "nlay = 100\n",
    "coords = {\"layer\": np.arange(1, 101), \"y\": [5.0], \"x\": x}\n",
    "dims = (\"layer\", \"y\", \"x\")\n",
    "\n",
    "bnd = xr.DataArray(\n",
    "    data=np.full((nlay, nrow, ncol), 1.0),\n",
    "    coords=coords,\n",
    "    dims=dims,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "k0 = 1.0 # m/d\n",
    "A = 0.001 # decay parameter\n",
    "z = np.arange(0.0, 1000.0, 10.0) + 5.0\n",
    "\n",
    "k_z = xr.DataArray(\n",
    "    data=conductivity(k0, z, A),\n",
    "    coords={\"z\": z},\n",
    "    dims=(\"z\"),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Let's inspect the result again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(13, 10))\n",
    "k_z.plot(y=\"z\")\n",
    "plt.title(\"Conductivity (m/d)\")\n",
    "plt.gca().invert_yaxis()\n",
    "plt.xlabel(\"x\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "iMODFLOW works with discrete layers, rather than elevation. Additionally, the top layer is the first rather than the last. Hence, we use the `layer` coordinate from the `bnd` array, flip it around, and then rename the `z` coordinate.\n",
    "\n",
    "Although `bnd` is a three-dimensional array, and `k_z` is a one-dimensional array, `xarray` can multiply using automatic broadcasting. Here, it means that `k_z` will be expanded to three dimensions to match `bnd`. `xarray` is able to do this because the `layer` dimension is present in both DataArrays, with identical values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k_z.coords[\"z\"] = bnd.coords[\"layer\"].values[::-1]\n",
    "k_z = k_z.rename({\"z\": \"layer\"})\n",
    "kh = bnd.copy() * k_z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Generate the iMODFLOW model files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we initialize an `OrderedDict` to hold the DataArrays and use `imod` to write the model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "model = OrderedDict()\n",
    "model[\"bnd\"] = bnd\n",
    "# constant head in the first layer\n",
    "model[\"bnd\"].sel(layer=1)[...] = -1.0\n",
    "model[\"kdw\"] = kh * 10.0\n",
    "model[\"vcw\"] = 10.0 / kh\n",
    "model[\"shd\"] = bnd * head_top\n",
    "# iMODFLOW won't run with just these packages, so we add dummy recharge.\n",
    "model[\"rch\"] = xr.full_like(bnd.sel(layer=1), 0.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "imod.write(\"toth\", model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "The `.write` function has written all necessary IDF files, and generated a runfile. \n",
    "\n",
    "Before running the model, you might want to edit the runfile for `kdw` and `vcw` so iMODFLOW will output flow budget files."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Load the results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`imod` makes it very use to load the results of all 100 layers. It takes a [globpath](https://docs.python.org/3/library/glob.html), so we can load all layers in a single statement.\n",
    "\n",
    "Here, we use the `*` symbol to denote a wildcard in the path to load all IDF files in the `results/head/` folder."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "head = imod.idf.load(\"toth/results/head/*.idf\")\n",
    "# Take a look at the DataArray:\n",
    "head"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(10, 6))\n",
    "head.isel(y=0).plot()\n",
    "plt.gca().invert_yaxis()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Look at streamlines"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`xarray` won't give us streamlines right out of the box. We'll use `matplotlib`'s streamplot functionality instead, by providing it the necessary (two-dimensional) `numpy` arrays. \n",
    "\n",
    "Once again, we can easily load the flow results using the `load` function; we select the first (and only) row using `.isel` to get a 2D array; we call `.values` to get a `numpy` array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "vz = imod.idf.load(\"toth/results/bdgflf/*.idf\").isel(y=0).values[:]\n",
    "vx = imod.idf.load(\"toth/results/bdgfrf/*.idf\").isel(y=0).values[:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's plot the phreatic head on top, and the flowlines underneath:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "f, (ax1, ax2) = plt.subplots(ncols=1, nrows=2, sharex=True, gridspec_kw={'height_ratios': [0.2, 0.8]}, figsize=(15, 5))\n",
    "ax1.plot(head_top[\"x\"], head_top.values, color=\"k\")\n",
    "ax1.set_ylabel(\"head(m)\", size=16)\n",
    "ax2.streamplot(x, z[:], vx, vz, arrowsize=2)\n",
    "ax2.invert_yaxis()\n",
    "ax2.set_ylabel(\"layer\", size=16)\n",
    "ax2.set_xlabel(\"x\", size=16)\n",
    "f.set_figheight(10.0)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
