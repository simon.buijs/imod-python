imod package
============

Submodules
----------

imod module
---------------

.. automodule:: imod.__init__
    :members:
    :undoc-members:
    :show-inheritance:

imod.idf module
---------------

.. automodule:: imod.idf
    :members:
    :undoc-members:
    :show-inheritance:

imod.ipf module
---------------

.. automodule:: imod.ipf
    :members:
    :undoc-members:
    :show-inheritance:

imod.rasterio module
--------------------

.. automodule:: imod.rasterio
    :members:
    :undoc-members:
    :show-inheritance:

imod.run module
--------------------

.. automodule:: imod.run
    :members:
    :undoc-members:
    :show-inheritance:

imod.tec module
---------------

.. automodule:: imod.tec
    :members:
    :undoc-members:
    :show-inheritance:

imod.util module
----------------

.. automodule:: imod.util
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: imod
    :members:
    :undoc-members:
    :show-inheritance:
